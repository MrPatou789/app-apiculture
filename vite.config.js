import { defineConfig } from 'vite'

import { resolve } from 'path'

import vue from '@vitejs/plugin-vue2'

export default defineConfig({
  plugins: [
    vue()
	],
  resolve: {
    alias: {
      '@': resolve(__dirname, "src/"),
    },
  },
})
