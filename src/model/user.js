import { nanoid } from 'nanoid'

export default class User {
  constructor(firstName, lastName, email, password) {
    this.id = nanoid();
    this.apiaries = [];
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  }

  get id() {
    return this._id;
  }

  get apiaries() {
    return this._apiaries;
  }

  get firstName() {
    return this._firstName;
  }

  get lastName() {
    return this._lastName;
  }

  get email() {
    return this._email;
  }

  get password() {
    return this._password;
  }

  set firstName(firstName) {
    this._firstName = firstName;
  }

  set id(id) {
    this._id = id;
  }

  set apiaries(apiaries) {
    this._apiaries = apiaries;
  }

  set lastName(lastName) {
    this._lastName = lastName;
  }

  set email(email) {
    this._email = email;
  }

  set password(password) {
    this._password = password;
  }
}
