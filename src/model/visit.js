import { nanoid } from 'nanoid'

export default class Visit {
  constructor(date, food, hausse, devDynamic, comments) {
    this.id = nanoid();
    this.date = date;
    this.food = food;
    this.hausse = hausse;
    this.devDynamic = devDynamic;
    this.comments = comments;
  }

  get id() {
    return this._id;
  }

  get date() {
    return this._date;
  }

  get food() {
    return this._food;
  }

  get hausse() {
    return this._hausse;
  }

  get devDynamic() {
    return this._devDynamic;
  }

  get comments() {
    return this._comments;
  }

  set id(id) {
    this._id = id;
  }

  set date(date) {
    this._date = date;
  }

  set food(food) {
    this._food = food;
  }

  set hausse(hausse) {
    this._hausse = hausse;
  }

  set devDynamic(devDynamic) {
    this._devDynamic = devDynamic;
  }

  set comments(comments) {
    this._comments = comments;
  }
}
