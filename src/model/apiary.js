import { nanoid } from 'nanoid'
import { format } from "date-fns";

export default class Apiary {
  constructor(name, nbBeehavior, environments, coordinate, frequency) {
    this.id = nanoid();
    this.visits = [];
    this.name = name;
    this.nbBeehavior = nbBeehavior;
    this.environments = environments;
    this.coordinate = coordinate;
    this.date = format(new Date(), "MM/dd/yyyy");
    this.frequency = frequency;
  }

  get id() {
    return this._id;
  }

  get visits() {
    return this._visits;
  }

  get name() {
    return this._name;
  }

  get nbBeehavior() {
    return this._nbBeehavior;
  }

  get environments() {
    return this._environments;
  }

  get coordinate() {
    return this._coordinate;
  }

  get frequency() {
    return this._frequency;
  }

  set id(id) {
    this._id = id;
  }

  set visits(visits) {
    this._visits = visits;
  }

  set name(name) {
    this._name = name;
  }

  set nbBeehavior(nbBeehavior) {
    this._nbBeehavior = nbBeehavior;
  }

  set environments(environments) {
    this._environments = environments;
  }

  set coordinate(coordinate) {
    this._coordinate = coordinate;
  }

  set date(date) {
    this._date = date;
  }

  set frequency(frequency) {
    this._frequency = frequency;
  }
}
