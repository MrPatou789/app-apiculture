import fr from "date-fns/locale/fr";
import { format, differenceInDays } from "date-fns";

export default {
  methods: {
    /**
     * @param {string} date
     * @return la date convertie au format voulue
     */
    convertDate(date) {
      return format(new Date(date), "EEEE d MMMM yyyy", { locale: fr });
    },

    /**
     * @param {object} apiary
     * @return la couleut du rucher spécifié
     */
    getApiaryColor(apiary) {
      const lastVisit = apiary._visits[0];
      const frequency = this.$store.getters["config/GET_FREQUENCY"];

      let diff = null;
      if (lastVisit) {
        diff = Math.abs(
          differenceInDays(new Date(lastVisit._date), new Date())
        );
      } else {
        diff = Math.abs(differenceInDays(new Date(apiary._date), new Date()));
      }

      if (diff > frequency) return "#ff0000";

      if (diff >= frequency / 2) return "#ff9500";

      return "#48c421";
    },

    /**
     * @param {object} apiaries
     * @return un tableau des rucher rouge
     */
    getRedApiary(apiaries) {
      const data = [];
      apiaries.forEach(apiary => {
        if (this.getApiaryColor(apiary) === "#ff0000") {
          data.push(apiary);
        }
      });

      return data.sort((a, b) => {
        return new Date(a._date) - new Date(b._date);
      });
    },

    /**
     * @param {object} apiaries
     * @return un tableau des rucher orange
     */
    getOrangeApiary(apiaries) {
      const data = [];
      apiaries.forEach(apiary => {
        if (this.getApiaryColor(apiary) === "#ff9500") {
          data.push(apiary);
        }
      });

      return data.sort((a, b) => {
        return new Date(a._date) - new Date(b._date);
      });
    },

    /**
     * @param {object} apiaries
     * @return un tableau des rucher vert
     */
    getGreenApiary(apiaries) {
      const data = [];
      apiaries.forEach(apiary => {
        if (this.getApiaryColor(apiary) === "#48c421") {
          data.push(apiary);
        }
      });

      return data.sort((a, b) => {
        return new Date(a._date) - new Date(b._date);
      });
    }
  }
};
