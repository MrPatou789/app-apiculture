export default {
  data() {
    return {
      user: null
    };
  },

  /**
   * Récupere l'utilisateur connécter depuis le store
   */
  created() {
    const userConnectedId = this.$store.getters["data/GET_USERCONNECTED"];
    if (userConnectedId) {
      this.user = this.$store.getters["data/GET_USERS"].filter(
        user => user._id === userConnectedId
      )[0];
    }
  },

  watch: {
    /**
     * Observe user et pour chaque changement update le store
     */
    user: {
      deep: true,
      handler() {
        if (this.user) {
          const data = this.$store.getters["data/GET_USERS"].map(user => {
            if (user.id === this.user._id) return this.user;
            return user;
          });
          this.$store.dispatch("data/SET_USERS", data);
        }
      }
    },

    /**
     * @param {string} userConnectedId
     * Set user à l'utilisateur donné en paramètre
     */
    getUserConnected(userConnectedId) {
      if (userConnectedId) {
        this.user = this.$store.getters["data/GET_USERS"].filter(
          user => user._id === userConnectedId
        )[0];
      } else {
        this.user = null;
      }
    }
  },

  computed: {
    /**
     * Observe le store
     * @return l'id de l'ulisateur connecté
     */
    getUserConnected() {
      return this.$store.getters["data/GET_USERCONNECTED"];
    }
  },

  methods: {
    /**
     * @return les coordonées de l'utilisateur
     */
    getCoords() {
      return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(resolve, reject);
      });
    }
  }
};
