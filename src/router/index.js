import Vue from "vue";
import VueRouter from "vue-router";
import Store from "../store/index";
import Home from "../views/Home.vue";
import Settings from "../views/back/settings.vue";
import ApiariesMap from "../views/front/apiaries_map.vue";
import ApiariyVisit from "../views/front/apiary_visit.vue";
import User from "../views/back/user.vue";
import Apiary from "../views/back/apiary.vue";
import Error404 from "../views/error/404.vue";
import Error403 from "../views/error/403.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      isHome: true
    }
  },
  {
    path: "/apiaries/map",
    name: "Apiaries.Map",
    component: ApiariesMap,
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/admin/settings",
    name: "Admin.Settings",
    component: Settings,
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/apiary/:id",
    name: "Apiary.Visit",
    component: ApiariyVisit,
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/admin/user",
    name: "Admin.User.New",
    component: User,
    meta: {
      requireAuth: false
    }
  },
  {
    path: "/admin/user/:id",
    name: "Admin.User.Edit",
    component: User,
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/admin/apiary",
    name: "Admin.Apiary.New",
    component: Apiary,
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/admin/apiary/:id",
    name: "Admin.Apiary.Edit",
    component: Apiary,
    meta: {
      requireAuth: true
    }
  },
  {
    path: "/404",
    name: "404",
    component: Error404,
    meta: {
      isPageError: true
    }
  },
  {
    path: "/403",
    name: "403",
    component: Error403,
    meta: {
      isPageError: true
    }
  },
  {
    path: "**",
    redirect: { name: "404" }
  }
];

const router = new VueRouter({
  mode: "history",
  base: import.meta.VITE_BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  const isConnected = await Store.getters["data/GET_USERCONNECTED"];
  if (
    to.matched.some(record => record.meta.requireAuth) &&
    !to.matched.some(record => record.meta.isHome) &&
    !isConnected
  ) {
    return next({ name: "403" });
  }
  if (
    !to.matched.some(record => record.meta.requireAuth) &&
    !to.matched.some(record => record.meta.isPageError) &&
    !to.matched.some(record => record.meta.isHome) &&
    isConnected
  ) {
    return next({ name: "403" });
  }
  return next();
});
export default router;
