/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
const state = {
  users: [],
  userConnected: null
};

const getters = {
  GET_USERS(state) {
    return state.users;
  },
  GET_USERCONNECTED(state) {
    return state.userConnected;
  }
};

const mutations = {
  SET_USERS(state, payload) {
    state.users = payload;
  },
  SET_USERCONNECTED(state, payload) {
    state.userConnected = payload;
  }
};

const actions = {
  SET_USERS({ commit }, payload) {
    commit("SET_USERS", payload);
  },
  SET_USERCONNECTED({ commit }, payload) {
    commit("SET_USERCONNECTED", payload);
  },
  CLEAR({ commit }) {
    commit("SET_USERS", []);
    commit("SET_USERCONNECTED", null);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
