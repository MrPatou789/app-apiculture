import config from './config'
import data from './data'

export default {
  config,
  data
};
