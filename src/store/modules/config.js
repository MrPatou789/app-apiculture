/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
const state = {
  frequency: 10,
  environments: []
};

const getters = {
  GET_FREQUENCY(state) {
    return state.frequency;
  },
  GET_ENVIRONMENTS(state) {
    return state.environments;
  }
};

const mutations = {
  SET_FREQUENCY(state, payload) {
    state.frequency = payload;
  },
  SET_ENVIRONMENTS(state, payload) {
    state.environments = payload;
  }
};

const actions = {
  SET_FREQUENCY({ commit }, payload) {
    commit("SET_FREQUENCY", payload);
  },
  SET_ENVIRONMENTS({ commit }, payload) {
    commit("SET_ENVIRONMENTS", payload);
  },
  CLEAR({ commit }) {
    commit("SET_FREQUENCY", 10);
    commit("SET_ENVIRONMENTS", []);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
