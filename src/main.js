import Vue from "vue";
import Snotify, { SnotifyPosition } from "vue-snotify";
import VueMeta from 'vue-meta'
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

Vue.use(VueMeta)

Vue.use(Snotify, {
  toast: {
    timeout: 2500,
    pauseOnHover: false,
    position: SnotifyPosition.rightTop
  }
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
